# Docker Image: [semperos/clojure](https://hub.docker.com/r/semperos/clojure/tags?page=1&ordering=last\_updated)

Hosted on [Docker Hub](https://hub.docker.com/). Designed for CI test and build jobs.

Currently includes:

- `clj`/`clojure`
- `clj-kondo` ([clj-kondo](https://github.com/clj-kondo/clj-kondo))
- `bb` ([babashka](https://github.com/babashka/babashka))

See the [Dockerfile](./Dockerfile) for current versions of these tools.

## Adding or Updating Tools

- Verify the installer scripts by hand.
- Run `sha256sum` locally on verified scripts for values used in `Dockefile`

## Docker Commands

A reminder to myself of the commands in play:

```shell
# Use API token as password
docker login -u <username>

# If you get an error about an unusable GPG public key, then do this
# and try logging in again:
gpg --quick-generate-key <email>
gpg --list-keys # copy the new key id
rm -rf ~/.password-store
pass init <gpg key id>
docker login -u <username>

# Review images
docker images

# Build image with specific tag
docker build -t semperos/clojure:tools-deps-1.10.3.905 .

# Clean up if needed. All docker containers, including stopped ones:
docker ps -a

# Remove a stopped container that's using an image:
docker rm <name-or-id>

# Remove an image:
docker rmi <id>

# Push desired image + tag
docker push semperos/clojure:tools-deps-1.10.3.905

# Clock out
docker logout
```

## Changelog

### 2021/09/20

- **Added:** babashka version [`0.6.1`](https://github.com/babashka/babashka/releases/tag/v0.6.1)
- **Updated:** Clojure CLI tools version `1.10.3.967`

### 2021/08/11

- **Added:** clj-kondo version [`2021.08.06`](https://github.com/clj-kondo/clj-kondo/releases/tag/v2021.08.06)
- **Updated:** Clojure CLI tools version `1.10.3.943`


### 2021/07/29

- Initial tag pushed to Docker Hub
- **Added:** Clojure CLI tools version `1.10.3.905`

## License

The MIT License (MIT)

Copyright © 2021 Daniel Gregoire

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
